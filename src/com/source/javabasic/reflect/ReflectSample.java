package com.source.javabasic.reflect;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * 功能描述：反射的实现例子
 * @author zhangzhl
 *
 */
public class ReflectSample {
	
	public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchFieldException {
		// 反射获取class的方式
		// 1、通过类名全路径获得：
		Class<?> clazz = Class.forName("com.source.javabasic.reflect.Robot");
		// 2、通过类名获得
		Class<Robot> clazz2 = Robot.class;
		// 3、通过实例对象获得
		Class<? extends Class>  clazz3 = clazz2.getClass();
		Robot r = (Robot) clazz.newInstance();
		// getDeclaredMethod能获取所有的方法，但是不能获取继承的方法和所实现的接口的方法
		Method m = clazz.getDeclaredMethod("throwHello", String.class);
		m.setAccessible(true);
		Object obj = m.invoke(r, "zzl");
		System.out.println("gethello " + obj);
		// getMethod只能获取public的方法和他继承的方法以及所实现的接口的方法
		Method ms = clazz.getMethod("sayHi", String.class);
		ms.invoke(r, "sas");
		// 通过获取到的class取得属性
		Field filed = clazz.getDeclaredField("name");
		filed.setAccessible(true);
		filed.set(r, "alice");
		ms.invoke(r, "sas");
		
	}

}
