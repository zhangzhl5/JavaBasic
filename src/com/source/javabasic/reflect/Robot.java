package com.source.javabasic.reflect;

public class Robot {
	private  String name;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void sayHi(String helloSentence) {
		
		System.out.println(helloSentence+" " + name);
	}
	
	private String throwHello(String tag) {
		return "hello " + tag;
		
	}

}
