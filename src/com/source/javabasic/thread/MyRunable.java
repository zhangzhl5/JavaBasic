package com.source.javabasic.thread;

import java.util.Date;

public class MyRunable  implements Runnable{

	@Override
	public void run() {
		for(int i = 0; i<10; i++) {
			System.out.println("MyRunable线程正在执行："+new Date().getTime());
		}
	}
}
