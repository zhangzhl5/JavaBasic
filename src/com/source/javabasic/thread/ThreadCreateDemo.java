package com.source.javabasic.thread;

import java.util.Date;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class ThreadCreateDemo {

	
	public static void main(String[] args) throws InterruptedException, ExecutionException {
		//1.创建自定义线程
        MyThread thread = new MyThread();
        thread.start();
        //2.主线程循环打印
        for (int i=0; i<10; i++){
        	String name = Thread.currentThread().getName();
        	Thread.currentThread().sleep(100);
            System.out.println("main主线程正在执行："+name+new Date().getTime());
        }
        
        Thread threadRunable = new Thread(new MyRunable());
        threadRunable.start();
        
        FutureTask task = new FutureTask(new MyCallable());
        Thread threadCallbale = new Thread(task);
        threadCallbale.start();
        System.out.println(task.get());
        System.out.println(task.isDone());
    }
}
