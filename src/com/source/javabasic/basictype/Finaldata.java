package com.source.javabasic.basictype;

import java.util.Map.Entry;

public class Finaldata extends Final {
	
	// 如下可知被final修饰map在子类中添加数据后其他子类也可以获取
	public static void main(String[] args) {
		EditFinal.addData();
		for(Entry<String, Integer> enrty :Final.map.entrySet()){
			enrty.getKey();
			enrty.getValue();
			System.out.println(	enrty.getKey() +" : "+ enrty.getValue());
		}
	}

}
