package com.source.javabasic.basictype;

import java.util.HashMap;
import java.util.Map.Entry;


/**
 * final修饰符的作用于解释
 */
public class Final {
	
	// 被final继续试
	public static final  HashMap<String, Integer> map  = new HashMap<String, Integer>();
	
	
	public static void main(String[] args) {
		Final.map.put("ds", 23);
		Final.map.put("sd", 233);
		for(Entry<String, Integer> enrty :Final.map.entrySet()){
			enrty.getKey();
			enrty.getValue();
			System.out.println(	enrty.getKey() +" : "+ enrty.getValue());
		}
		System.out.println(map.keySet());
	}
	
	public final void play() {
		System.out.println("final修饰的方法是不可以被（override）重写的");
	}

}
