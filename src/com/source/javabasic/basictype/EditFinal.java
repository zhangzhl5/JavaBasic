package com.source.javabasic.basictype;


/**
 * final修饰符的作用
 *
 */
//如果父类 被final修饰 则不可以被继承 提示如下：The type EditFinal cannot subclass the final class Final
public class EditFinal extends Final {
	
	public static void main(String[] args) {
		Final.map.put("sds", 34);
		Final.map.remove("ss");
		System.out.println(map.keySet());
		// 如下代码会提示：The final field Final.map cannot be assigned 所以被final修饰的map不能被重新指派，但是可以增删数据
		// Final.map = new HashMap<String,Integer>();
	}
	
	// 下面的方法不能重写：Cannot override the final method from Fina
//	public  void play() {
//		System.out.println("final修饰的方法是不可以被（override）重写的");
//	}
	
	public static void addData() {
		Final.map.put("sds", 34);
		Final.map.remove("ss");
	}
	
	
}
