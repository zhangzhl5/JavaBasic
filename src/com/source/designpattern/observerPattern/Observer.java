package com.source.designpattern.observerPattern;

public interface Observer {
	
	public void update(float temp,float humidity,float pressure);
	

}
