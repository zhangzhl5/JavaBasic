package com.source.designpattern.observerPattern;

public class WeatherStation {
	
	public static void main(String[] args) {
		WeatherData w = new WeatherData();
		CurrentConditionsDisplay dis = new CurrentConditionsDisplay(w);
		w.setmeasurements(123, 23, 520);
	}

}
