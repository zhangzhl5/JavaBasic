package com.source.designpattern.observerPattern;

public class CurrentConditionsDisplay implements Observer, DisplayElement {
	
	
	private float temperature;
	private float humidity;
	private Subject weatherdata;
	
	public CurrentConditionsDisplay (Subject weatherdata) {
		this.weatherdata = weatherdata;
		weatherdata.registerObserbver(this);
	}

	@Override
	public void display() {
		System.out.println(" CurrentConditionsDisplay "+ temperature+" F degrees and "+humidity+"%");

	}

	@Override
	public void update(float temp, float humidity, float pressure) {
		this.temperature = temp;
		this.humidity = humidity;
		display();

	}

}
