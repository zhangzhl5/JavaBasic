package com.source.designpattern.observerPattern;

import java.util.ArrayList;

/**
 * 观察者模式 = 出版者（主题）+ 订阅者（观察者）
 * 天气预报类
 * @author zhangzhl
 *
 */
public class WeatherData implements Subject{
	// 温度
	private float temperature;
	// 湿度
	private float humidity;
	// 气压
	private float pressure;
	//
	private ArrayList<Observer> observers;
	
	public WeatherData() {
		observers = new ArrayList<Observer>();
	}
	
	public float getTemperature() {
		return temperature;
	}
	public void setTemperature(float temperature) {
		this.temperature = temperature;
	}
	public float getHumidity() {
		return humidity;
	}
	public void setHumidity(float humidity) {
		this.humidity = humidity;
	}
	public float getPressure() {
		return pressure;
	}
	public void setPressure(float pressure) {
		this.pressure = pressure;
	}
	
	@Override
	public void registerObserbver(Observer o) {
		observers.add(o);
		
	}
	@Override
	public void removeObserbver(Observer o) {
		observers.remove(o);		
	}
	@Override
	public void notifyObservers() {
		for(int i = 0; i < observers.size(); i++) {
			Observer o = (Observer) observers.get(i);
			o.update(temperature, humidity, pressure);
		}
		
	}
	
	public void measurementsChanged() {
		notifyObservers();
	}
	
	public void setmeasurements(float temp,float humidity,float pressure) {
		this.temperature = temp;
		this.humidity = humidity;
		this.pressure = pressure;
		measurementsChanged();
	}
	
	
	

}
