package com.source.designpattern.observerPattern;

public interface DisplayElement {
	
	public void display();

}
