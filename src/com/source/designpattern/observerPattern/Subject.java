package com.source.designpattern.observerPattern;

public interface Subject {
	
	public void registerObserbver(Observer o);
	
	public void removeObserbver(Observer o);
	
	public void notifyObservers();

}
