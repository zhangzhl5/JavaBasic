package com.source.designpattern.adapterPattern;

/**
 * 接口描述：高级的播放器接口
 * @author Administrator
 *
 */
public interface AdvancedMediaPlayer {

	public void playVlc(String fileName);

	public void playMp4(String fileName);

}
