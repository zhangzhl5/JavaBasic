package com.source.designpattern.adapterPattern;

/**
 * 类描述：音频播放器实现类
 * @author Administrator
 *
 */
public class AudioPlayer implements MediaPlayer {

   //通过适配器在只能播放MP3的基础上增加了别的接口的实现方法
   MediaAdapter mediaAdapter; 
 
   @Override
   public void play(String audioType, String fileName) {    
      //播放 mp3 音乐文件的内置支持
      if(audioType.equalsIgnoreCase("mp3")){
         System.out.println("Playing mp3 file. Name: "+ fileName);         
      } 
      //mediaAdapter 提供了播放其他文件格式的支持
      else if(audioType.equalsIgnoreCase("vlc") 
         || audioType.equalsIgnoreCase("mp4")){
         mediaAdapter = new MediaAdapter(audioType);
         mediaAdapter.play(audioType, fileName);
      }
      else{
         System.out.println("Invalid media. "+
            audioType + " format not supported");
      }
   }   
}