package com.source.designpattern.adapterPattern;

/**
 * 类描述：MediaPlayer的适配器 实现了基础的播放器接口，通过成员变量注入了高级播放器的实现
 * 
 * @author Administrator
 * 
 */
public class MediaAdapter implements MediaPlayer {

	// 高级的媒体播放器接口
	AdvancedMediaPlayer advancedMusicPlayer;

	/**
	 * 构造函数，根据不同的类型初始化不同的接口实现类
	 * @param audioType
	 */
	public MediaAdapter(String audioType) {
		if (audioType.equalsIgnoreCase("vlc")) {
			// 实现高级播放器接口的VlcPlayer
			advancedMusicPlayer = new VlcPlayer();
		} else if (audioType.equalsIgnoreCase("mp4")) {
			// 实现高级播放器接口的Mp4Player
			advancedMusicPlayer = new Mp4Player();
		}
	}

	@Override
	public void play(String audioType, String fileName) {
		if (audioType.equalsIgnoreCase("vlc")) {
			advancedMusicPlayer.playVlc(fileName);
		} else if (audioType.equalsIgnoreCase("mp4")) {
			advancedMusicPlayer.playMp4(fileName);
		}
	}
}