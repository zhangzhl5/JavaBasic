package com.source.designpattern.adapterPattern;
public class AdapterPatternDemo {
   public static void main(String[] args) {
	  // 这样通过适配器就可以在一个实现类里实现不同类型的play方法
      AudioPlayer audioPlayer = new AudioPlayer();
 
      audioPlayer.play("mp3", "beyond the horizon.mp3");
      audioPlayer.play("mp4", "alone.mp4");
      audioPlayer.play("vlc", "far far away.vlc");
      audioPlayer.play("avi", "mind me.avi");
   }
}